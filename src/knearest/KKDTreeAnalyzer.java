package knearest;

import java.util.ArrayList;
import java.util.List;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeySizeException;

public class KKDTreeAnalyzer {
	private double[] sourceKey;
	private KDTree<KObject> tree;
	private int k = 3;
	
	public KKDTreeAnalyzer(KObject origin, KDTree<KObject> tree) {
		this.sourceKey = new double[2];
		this.sourceKey[0] = origin.getX();
		this.sourceKey[1] = origin.getY();
		this.tree = tree;
	}
	
	public KKDTreeAnalyzer(KObject origin, KDTree<KObject> tree, int k) {
		this.sourceKey = new double[2];
		this.sourceKey[0] = origin.getX();
		this.sourceKey[1] = origin.getY();
		this.k = k;
		this.tree = tree;
	}
	
	public List<KObject> nearest() {
		List<KObject> objects = new ArrayList<KObject>();
		try {
			objects = tree.nearest(sourceKey, k);
		} catch (KeySizeException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		return objects;
	}
}
