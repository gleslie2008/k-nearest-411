package knearest;

public class KObject {
	private float x;
	private float y;
	private int id;
	private float distance;
	
	public KObject() { }
	
	public KObject(int id, float x, float y) {
		this.x = x;
		this.y = y;
		this.id = id;
	}
	
	public void setDistance(float distance) {
		this.distance = distance;
	}
	
	public float getX() { return x; }
	public float getY() { return y; }
	public int getId() { return id; }
	public float getDistance() { return distance; }
	public float distanceTo(KObject other) {
		return (float) Math.sqrt(Math.pow(x - other.getX(), 2) + Math.pow(y - other.getY(), 2));
	}
}
