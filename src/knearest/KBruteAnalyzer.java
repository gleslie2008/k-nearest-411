package knearest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class KBruteAnalyzer {
	private KObject origin;
	private List<KObject> points;
	private int k = 3;
	
	public KBruteAnalyzer() { }
	public KBruteAnalyzer(KObject origin, List<KObject> points) {
		this.origin = origin;
		this.points = points;
	}
	public KBruteAnalyzer(KObject origin, List<KObject> points, int k) {
		this.origin = origin;
		this.points = points;
		this.k = k;
	}
	
	public List<KObject> nearest() {
		TreeMap<Float, KObject> resultSet = new TreeMap<Float, KObject>();
		
		Iterator<KObject> iter = points.iterator();
		while (iter.hasNext()) {
			KObject point = iter.next();
			resultSet.put(distance(origin, point), point);
		}
		
		List<KObject> results = new ArrayList<KObject>();
		int k = this.k;
		while (k > 0) {
			float firstKey = resultSet.firstKey();
			KObject object = resultSet.get(firstKey);
			object.setDistance(firstKey);
			results.add(object);
			resultSet.remove(firstKey);
			k--;
		}
	
		return results;
	}
	
	private float distance(KObject source, KObject dest) {
		return source.distanceTo(dest);
	}
}
