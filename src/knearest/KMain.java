package knearest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeyDuplicateException;
import edu.wlu.cs.levy.CG.KeySizeException;

public class KMain {
	public static void main(String args[]) {
		
		/* begin trials */
		trialBrute("100.csv", 50);
		trialBrute("1000.csv", 50);
		trialBrute("10000.csv", 50);
		trialBrute("100000.csv", 50);
		trialBrute("1000000.csv", 50);

		trialKD("100.csv", 50);
		trialKD("1000.csv", 50);
		trialKD("10000.csv", 50);
		trialKD("100000.csv", 50);
		trialKD("1000000.csv", 50);
		
	}
	
	/* prints the time of a brute force test */
	public static void trialBrute(String path, int k) {
		System.out.println("BRUTE: Testing file[" + path + "] for k[" + k + "]");
		System.out.println("BRUTE: Execution Time: " + testBrute(path, k) + " ns");
	}
	
	/* prints the time of a kdtree test */
	public static void trialKD(String path, int k) {
		System.out.println("KD: Testing file[" + path + "] for k[" + k + "]");
		System.out.println("KD: Execution Time: " + testKD(path, k) + " ns");
	}
	
	/* returns the time of a brute force test */
	public static long testBrute(String path, int k) {
		List<KObject> objects = null;
		path = Paths.get("").toAbsolutePath().toString() + "/" + path;
				
		try {
			KLoader loader = new KLoader(path);
			objects = loader.getObjects();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		assert objects != null;
		
		KObject source = objects.remove(0);
		
		final long startTime = System.nanoTime();
		KBruteAnalyzer analyzer = new KBruteAnalyzer(source, objects, k);
		analyzer.nearest();
		final long endTime = System.nanoTime();

		return endTime - startTime;
	}
	
	/* returns the time of a KDTree test */
	public static long testKD(String path, int k) {
		List<KObject> objects = null;
		path = Paths.get("").toAbsolutePath().toString() + "/" + path;
				
		try {
			KLoader loader = new KLoader(path);
			objects = loader.getObjects();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		assert objects != null;
		
		KDTree<KObject> tree = new KDTree<KObject>(2);
		Iterator<KObject> iter = objects.iterator();
		while (iter.hasNext()) {
			KObject object = iter.next();
			double[] dimensions = new double[2];
			dimensions[0] = object.getX();
			dimensions[1] = object.getY();
			try {
				tree.insert(dimensions, object);
			} catch (KeySizeException e) {
				e.printStackTrace();
			} catch (KeyDuplicateException e) {
				e.printStackTrace();
			}
		}
		
		KObject source = objects.remove(0);
		
		final long startTime = System.nanoTime();
		KKDTreeAnalyzer analyzer = new KKDTreeAnalyzer(source, tree, k);
		List<KObject> list = analyzer.nearest();
		final long endTime = System.nanoTime();

		return endTime - startTime;
	}
}
