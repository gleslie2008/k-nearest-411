package knearest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class KLoader {
	private List<KObject> objects;
	
	public KLoader(String filename) throws FileNotFoundException, IOException {
		objects = new ArrayList<KObject>();
		
		BufferedReader br = new BufferedReader(new FileReader(filename));
	    try {
	        String line = br.readLine();

	        while (line != null) {
	            line = br.readLine();
	            
	            if (line == null) return;
	            
	            String[] contents = line.split(",");
	            
	            int id = Integer.valueOf(contents[0]);
	            float lat = Float.valueOf(contents[1]);
	            float lng = Float.valueOf(contents[2]);
	            
	            objects.add(new KObject(id, lat, lng));
	        }
	    } finally {
	        br.close();
	    }
	}
	
	public List<KObject> getObjects() { return objects; }
}
