package knearest;

import java.util.Iterator;
import java.util.List;

public class KPrinter {
	private KObject origin;
	private List<KObject> points;
	
	public KPrinter() { }
	public KPrinter(KObject origin, List<KObject> points) {
		this.origin = origin;
		this.points = points;
	}
	
	public void print() {
		System.out.println("source: " + origin.getId());
		System.out.println("--------------------------");
		Iterator<KObject> iter = points.iterator();
		int count = 1;
		while (iter.hasNext()) {
			KObject object = iter.next();
			System.out.println(count + ": " + object.getId() + " is " + object.getDistance() + " units away");
			count++;
		}
	}
}
